package zad1;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Base64;


public class Client extends Thread {
	
	private String host;
	private int port;
	private ClientGui gui;
	private SocketChannel channel;
	private final Charset charset = Charset.forName("ISO-8859-2");
	private ByteBuffer byteBufferIn = ByteBuffer.allocate(1024); 
	private static int userCalc = 0;
	private String user;
	private final AESCipher aesCipher = new AESCipher("secret_key_pass1");
	private boolean isClientRunning = true;
	
	
	
	public Client(String host, int port) {
		setHost(host);
		setPort(port);
		setUserCalc(getUserCalc() + 1);
		setUser("User " + getUserCalc() + ": ");
		setGui(new ClientGui());
		
		addGuiListeners();
	}
	
	
	private void addGuiListeners() {
		
		//send message to server after clicking the Send button
		getGui().addSendButtonListener(e -> {
			sendMessage(getGui().getMessageBoxText());
		});
		
		
		//Log on/Log Off client to/from server
		getGui().addLogOnOffListener(e -> {
			//log on
			if(getGui().getLogOnOffButtonCaption().equals("Log on")) {
				try {
					setChannel(SocketChannel.open());
					getChannel().configureBlocking(false);
					connect();					
				} catch (IOException e1) {
					System.out.println("I/O error during connection");
					e1.printStackTrace();
				}
			//log off
			} else {
				logOff();
			}
			
		});
	}
	
	
	public void connect() {
		try {
			if(!channel.isOpen()) {
					setChannel(SocketChannel.open());
				}
			
			getChannel().connect(new InetSocketAddress(host, port));
			System.out.println(getUser() + " Connecting ...");
			while(!getChannel().finishConnect()) {
			}
			
			System.out.println(getUser() + " Connected");
			getGui().setLogOnOffButtonCaption("Log off");
			start();
		} catch (IOException e) {
			System.out.println("I/O error during connection");
			e.printStackTrace();
		}
		
	}
	
	
	
	public void run() {
		StringBuffer answer = new StringBuffer();
		String finalAnswer = null;
		CharBuffer charBufferIn = null;
		
		try {	
			while(isClientRunning) {
				getByteBufferIn().clear();													//clear buffer, change to write mode
				int bytesRead = getChannel().read(getByteBufferIn());
				if(bytesRead > 0) {
					getByteBufferIn().flip();												//change to read mode
					charBufferIn = getCharset().decode(getByteBufferIn());					//change ByteBuffer to CharBuffer (message still encoded in Base64 and encrypted)
					answer.append(charBufferIn);											//concatenate encrypted message encoded in Base64
				} else if(bytesRead == -1) {
					logOff();
					isClientRunning = false;
				} else {
					if(answer.length() > 0) {
						try {
							byte[] byteArr = Base64.getDecoder().decode(answer.toString());	//decode message encoded in Base64 (but still encrypted)
							finalAnswer = getAesCipher().decrypt(ByteBuffer.wrap(byteArr));	//decrypt message from AES CBC
							finalAnswer = finalAnswer.replaceAll("\\n", "\n    ");			//add indent in message in GUI
							getGui().setHistoryAreaText(finalAnswer + "\n\n");
						} catch (Exception e) {
							System.out.println("Error during decryption");
							getGui().setHistoryAreaText("!!!Message Lost!!!\n");
							e.printStackTrace();
						}
						answer.setLength(0);												//clean up message container
					}
				}
			}
		} catch (IOException e) {
			logOff();
			System.out.println(getUser());
			e.printStackTrace();
		}
	}

	
	public void logOff() {
		try {
			setIsClientRunning(false);
			getChannel().close();
			getChannel().socket().close();
			getGui().setLogOnOffButtonCaption("Log on");
			System.out.println(getUser() + " Disconnected");
		} catch (IOException e) {
			System.out.println("Log off error");
			e.printStackTrace();
		}
	}
	
	
	public void sendMessage(String message) {
		try {
			message = getUser() + message;													//add user name to message
			ByteBuffer byteBuffer = getAesCipher().encrypt(message);						//encrypt message in AES CBC
			ByteBuffer byteBufferOut = Base64.getEncoder().encode(byteBuffer);				//encode message in Base64
			getChannel().write(byteBufferOut);												//send message to server
		} catch (IOException e) {
			System.out.println("I/O error during sending message");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error during encryption");
			e.printStackTrace();
		}
	}

	
	
//host
	public String getHost() {
		return host;
	}


	public void setHost(String host) {
		this.host = host;
	}


//port
	public int getPort() {
		return port;
	}


	public void setPort(int port) {
		this.port = port;
	}

	
//gui
	public ClientGui getGui() {
		return gui;
	}


	public void setGui(ClientGui gui) {
		this.gui = gui;
	}


//channel
	public SocketChannel getChannel() {
		return channel;
	}


	public void setChannel(SocketChannel channel) {
		this.channel = channel;
	}

	
//charset
	public Charset getCharset() {
		return charset;
	}

	
//byteBufferIn
	public ByteBuffer getByteBufferIn() {
		return byteBufferIn;
	}


	public void setByteBufferIn(ByteBuffer byteBufferIn) {
		this.byteBufferIn = byteBufferIn;
	}

	
//userCalc
	public static int getUserCalc() {
		return userCalc;
	}



	public static void setUserCalc(int userCalc) {
		Client.userCalc = userCalc;
	}


//user
	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}
	

//aesCipher
	public AESCipher getAesCipher() {
		return aesCipher;
	}
	
	
	
///IsClientRunning
	public boolean getIsClientRunning() {
		return isClientRunning;
	}


	public void setIsClientRunning(boolean isClientRunning) {
		this.isClientRunning = isClientRunning;
	}


	
	
	
	public static void main(String[] args) {
		for(int i = 0; i < 2; i++) {
			new Client("localhost", 50000);
		}
	}
}
