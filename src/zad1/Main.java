/**
 *
 *  @author Sterniński Przemysław S14665
 *
 */

package zad1;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

  public static void main(String[] args) {
	  
	  //seriver
	  ExecutorService executor = Executors.newSingleThreadExecutor();
	  	executor.submit(() -> {
	  		new Server("localhost", 50000);
	  	});
	  
	  
	  //clients
	  for(int i = 0; i < 2; i++) {
		new Client("localhost", 50000);
	  }
	  
  }
}
