package zad1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ClientGui extends JFrame {
	private JTextArea historyArea = new JTextArea(20, 20);
	private JTextArea messageBox = new JTextArea(4, 20);
	private JButton sendButton = new JButton("Send");
	private JButton logOnOffButton = new JButton("Log on");
	
	
	public ClientGui() {
		// Konfiguracja GUI
		
		Font font = new Font("Dialog", Font.BOLD, 14);
		getHistoryArea().setFont(font);
		getMessageBox().setFont(font);
		getMessageBox().setBorder(BorderFactory.createLineBorder(Color.orange, 1));
		getHistoryArea().setEditable(false);
		
		JPanel p = new JPanel(new BorderLayout());
		p.add(new JScrollPane(getMessageBox()), "Center");
		p.add(getSendButton(), "East");
		p.add(getLogOnOffButton(), "South");
		
		Container cp = getContentPane();
		cp.add(new JScrollPane(getHistoryArea()), "Center");
		cp.add(p, "South");
		
	    pack();
	    show();
	}
	
	
//SendButtonListener
	public void addSendButtonListener(ActionListener action) {
		sendButton.addActionListener(action);
	}
	
	
//LogOnOffListener
	public void addLogOnOffListener(ActionListener action) {
		logOnOffButton.addActionListener(action);
	}


//HistoryArea	
	public String getHistoryAreaText() {
		return historyArea.getText();
	}


	public void setHistoryAreaText(String text) {
		historyArea.append(text);
		historyArea.setCaretPosition(historyArea.getText().length());
	}


	
	private JTextArea getHistoryArea() {
		return historyArea;
	}


	private void setHistoryArea(JTextArea historyArea) {
		this.historyArea = historyArea;
	}


//MessageBox
	public String getMessageBoxText() {
		return messageBox.getText();
	}


	public void setMessageBoxText(String text) {
		messageBox.setText(text);
	}

	
	
	private JTextArea getMessageBox() {
		return messageBox;
	}


	private void setMessageBox(JTextArea messageBox) {
		this.messageBox = messageBox;
	}


//SendButton
	private JButton getSendButton() {
		return sendButton;
	}

	
	private void setSendButton(JButton sendButton) {
		this.sendButton = sendButton;
	}
	

//LogOnOffButton
	public String getLogOnOffButtonCaption() {
		return logOnOffButton.getText();
	}
	
	
	public void setLogOnOffButtonCaption(String caption) {
		logOnOffButton.setText(caption);
	}


	
	public JButton getLogOnOffButton() {
		return logOnOffButton;
	}


	public void setLogOnOffButton(JButton logOnOffButton) {
		this.logOnOffButton = logOnOffButton;
	}


}