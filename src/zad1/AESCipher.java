package zad1;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;



public class AESCipher {
	
	private final Charset charset = Charset.forName("ISO-8859-2");
	private SecretKey key;
	private static final int IV_LENGTH = 16;
	
	public AESCipher(String keyString){
			//KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
			//setKey(keyGenerator.generateKey());
			key = new SecretKeySpec(keyString.getBytes(), "AES");
	}
	
	
	private byte[] createInitializationVector() {
		SecureRandom randomGenerator = new SecureRandom();
		byte[] iv = new byte[getIvLength()];
		randomGenerator.nextBytes(iv);
		return iv;
	}
	
	
	public ByteBuffer encrypt(String message) 
			throws NoSuchAlgorithmException
					, InvalidKeyException
					, InvalidAlgorithmParameterException
					, UnsupportedEncodingException
					, IllegalBlockSizeException
					, BadPaddingException
					, NoSuchPaddingException {
		
		byte[] iv = createInitializationVector();
		
		Cipher encryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		encryptCipher.init(Cipher.ENCRYPT_MODE, getKey(), new IvParameterSpec(iv));

		byte[] messageByteArray = message.getBytes(getCharset());
	    byte[] encryptedByteArray = encryptCipher.doFinal(messageByteArray);

	    ByteBuffer encryptedByteBuffer = ByteBuffer.allocate(iv.length + encryptedByteArray.length);
		encryptedByteBuffer.put(iv);																	//add initialization vector to ByteBuffer
		encryptedByteBuffer.put(encryptedByteArray);													//add encrypted message to ByteBuffer
		encryptedByteBuffer.flip();																		//change ByteBuffer to read mode
		
		return encryptedByteBuffer;
	}
	
	
	public String decrypt(ByteBuffer ecryptedByteBuffer) 
			throws NoSuchAlgorithmException
					, InvalidKeyException
					, InvalidAlgorithmParameterException
					, UnsupportedEncodingException
					, IllegalBlockSizeException
					, BadPaddingException
					, NoSuchPaddingException {
		
		byte[] iv = new byte[getIvLength()];
		ecryptedByteBuffer.get(iv);												//extract initailization vector from ecrypted ByteBuffer
		
		byte[] encryptedByteArray = new byte[ecryptedByteBuffer.remaining()];
		ecryptedByteBuffer.get(encryptedByteArray);								//extract encrypted message from ecrypted ByteBuffer
		
		Cipher decryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		decryptCipher.init(Cipher.DECRYPT_MODE, getKey(), new IvParameterSpec(iv));
		String message = new String(decryptCipher.doFinal(encryptedByteArray), getCharset());

		return message;
	}


//charset
	public Charset getCharset() {
		return charset;
	}
	
	
//key
	public SecretKey getKey() {
		return key;
	}


	public void setKey(SecretKey key) {
		this.key = key;
	}

	
//IV_LENGTH
	public static int getIvLength() {
		return IV_LENGTH;
	}
	
	
}
