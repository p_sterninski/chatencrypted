/**
 *
 *  @author Sterniński Przemysław S14665
 *
 */

package zad1;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
	
	private ServerSocketChannel serverChannel;
	private Selector selector;
	private static Charset charset  = Charset.forName("ISO-8859-2");
	
	public Server(String host, int port) {
		try {
			setServerChannel(ServerSocketChannel.open());
			getServerChannel().configureBlocking(false);
			getServerChannel().socket().bind(new InetSocketAddress(host, port));
			setSelector(Selector.open());
			getServerChannel().register(getSelector(), SelectionKey.OP_ACCEPT);
		} catch (IOException e) {
			System.out.println("I/O error during server registration");
			e.printStackTrace();
		}
		
		System.out.println("Server started");
		serviceConnections();
	}
	
	
	public void serviceConnections() {
		boolean serverIsRunning = true;
		
		while(serverIsRunning) {
			try {
				getSelector().select();
				Set<SelectionKey> keys = getSelector().selectedKeys();
				Iterator<SelectionKey> iterator = keys.iterator();
				
				while(iterator.hasNext()) {
					SelectionKey key = iterator.next();
					iterator.remove();
					
					if(key.isAcceptable()) {
						registerClient();
						continue;
					}
					
					if(key.isReadable()) {
						readFromClient(key);
						continue;
					}
				}
			} catch (IOException e) {
				System.out.println("I/O error during clients selection for operation");
				e.printStackTrace();
				continue;
			}
		}
		
		
		closeServer();

	}
	
	
	public void registerClient() {
		SocketChannel socketChannel;
		try {
			socketChannel = getServerChannel().accept();
			socketChannel.configureBlocking(false);
			socketChannel.register(getSelector(), SelectionKey.OP_READ);
		} catch (IOException e) {
			System.out.println("Client registration failed");
			e.printStackTrace();
		}
	}
	
	
	
	public void readFromClient(SelectionKey key) {
		SocketChannel socketChannel = (SocketChannel) key.channel();
		if(!socketChannel.isOpen()) {
			return;
		}
		
		ByteBuffer byteBufferIn = ByteBuffer.allocate(1024); 
		
		StringBuffer message = new StringBuffer();
		CharBuffer charBuffer = null;
		
		byteBufferIn.clear();												//clear buffer, change to write mode
		int bytesRead = 0;
		try {
			while ((bytesRead = socketChannel.read(byteBufferIn)) > 0) {
				byteBufferIn.flip(); 										//read mode
				charBuffer = getCharset().decode(byteBufferIn); 			//change encrypted message encoded in Base64 to CharBuffer
				message.append(charBuffer); 									//concatenate encrypted message encoded in Base64
				byteBufferIn.clear(); 										//clear buffer, change to write mode
			}
			
			if(bytesRead == 0) {
				sendToClients(message.toString());								//when channel gets empty, send obtained message (encoded in Base64)
			} else if(bytesRead == -1) {
				socketChannel.close();
				System.out.println("SocketChannel Closed");
			}
		} catch (IOException e) {
			System.out.println("I/O error");
			e.printStackTrace();
		}
	}
	
	
	public void sendToClients(String message) {
		CharBuffer charBufferOut = CharBuffer.wrap(message);
		ByteBuffer byteBufferOut = charset.encode(charBufferOut);				//change whole message into bytes (ByteBuffer)- still encoded in Base64
		
		Set<SelectionKey> keys = getSelector().keys();
		Iterator<SelectionKey> iterator = keys.iterator();
		
		//iterate through all clients
		while(iterator.hasNext()) {
			SelectionKey key = iterator.next();
			if(key.isValid() && key.channel() instanceof SocketChannel) {
				SocketChannel socketChannel = (SocketChannel) key.channel();
				try {
					socketChannel.write(byteBufferOut);							//send message to client
					byteBufferOut.rewind();										//in order to send the same message to next client, ByteBuffer needs to be rewind to beginning
				} catch (IOException e) {
					System.out.println("I/O error");
					e.printStackTrace();
				}
			}
		}
	}
	
	
	
	public void closeServer() {
		try {
			getServerChannel().close();
		} catch (IOException e) {
			System.out.println("I/O error during closing server");
			e.printStackTrace();
		}
	}
	
	
//serverChannel
	public ServerSocketChannel getServerChannel() {
		return serverChannel;
	}


	public void setServerChannel(ServerSocketChannel serverChannel) {
		this.serverChannel = serverChannel;
	}


//selector	
	public Selector getSelector() {
		return selector;
	}


	public void setSelector(Selector selector) {
		this.selector = selector;
	}
	

//charset
	public static Charset getCharset() {
		return charset;
	}


	public static void setCharset(Charset charset) {
		Server.charset = charset;
	}


	
	
	public static void main(String[] args) {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		executor.submit(() -> {
			new Server("localhost", 50000);
		});
	}
}
